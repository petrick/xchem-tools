import sys
import pathlib
import numpy as np
import scipy.io

import utils


def get_overlap(config):
    ov = utils.read_overlap(config['ov'])
    lm_map = utils.get_basis_map(config['basis_info'])
    for lm, basfuncs in lm_map.items():
        l, m = lm
        if l > max(config['lmono']):
            for ind in [bas.index + config['npoly_basis'] for bas in basfuncs]:
                ov[:, ind] = 0.
                ov[ind, :] = 0.
    return ov


def build_overlap(overlap, subspace1, subspace2=None):
    """
    Given the overlap matrix of a space generate the overlap matrix
    between two subspace given each by an iterable of indices.
    If subspace2 is not supplied, subspace2 = subspace1 (squared overlap)
    """
    if subspace2 is None:
        subspace2 = subspace1
    ov_shape = (len(subspace1), len(subspace2))
    sub_ov = np.zeros(ov_shape)
    for i in range(ov_shape[0]):
        for j in range(ov_shape[1]):
            sub_ov[i, j] = overlap[subspace1[i], subspace2[j]]
    return sub_ov


def get_projector(overlap, subspace):
    proj_ov = build_overlap(overlap, subspace)
    proj_basis = utils.orthonormalize_basis(proj_ov, 1e-8)
    return proj_basis @ proj_basis.T


def get_kmax(l, k_list):
    if len(k_list) == 1:
        kmax = k_list[0]
    else:
        kmax = k_list[l]
    return kmax


def project_poly_to_mono(xchem_data, overlap, orbitals, lm_map):
    projected_orbs = np.zeros(orbitals.shape)
    for l in range(max(xchem_data['lmono'])+1):
        kmax = get_kmax(l, xchem_data['kmono'])
        for m in range(-l, l+1):
            indices = [bas.index + xchem_data['npoly_basis'] for bas in lm_map[(l, m)] if bas.k <= kmax]
            lm_projector = get_projector(overlap, indices)
            mono_poly_ov = build_overlap(overlap, indices, range(xchem_data['npoly_basis']))
            projected = lm_projector @ mono_poly_ov @ orbitals[0:xchem_data['npoly_basis'], :]
            for i, ibas in enumerate(indices):
                projected_orbs[ibas, :] += projected[i, :]
    return projected_orbs[xchem_data['npoly_basis']:, :]


def transform_orbitals(xchem_data, overlap, orbitals, lm_map):
    transformed_orbs = np.copy(orbitals)
    projected_poly = project_poly_to_mono(xchem_data, overlap, orbitals, lm_map)
    transformed_orbs[xchem_data['npoly_basis']:, :] += projected_poly
    transformed_orbs[0:xchem_data['npoly_basis'], :] = 0.
    return transformed_orbs


def main():
    config = utils.get_config(' '.join(sys.argv[1:]))
    npoly_bas = config['npoly_basis']
    npoly_orbs = config['npoly_orbs']

    ov = get_overlap(config)
    orbs = utils.read_orbitals(config['orbs'][0])
    lm_map = utils.get_basis_map(config['basis_info'])

    new_orbs = transform_orbitals(config, ov, orbs, lm_map)
    np.savetxt('norms.txt', np.diag(new_orbs.T @ ov @ new_orbs))
    utils.write_orbitals_to_mpcg(config['path'] / 'mpcg_corr.bin', new_orbs)


if __name__ == '__main__':
    main()
