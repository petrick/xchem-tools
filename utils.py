"""
Utility functions to deal with orbitals and basis
from a xchem calculation
"""
import argparse
import collections
import json
import pathlib

import numpy as np
import scipy.io


def get_config(cmd=None):
    """Get config parameters from command line"""
    parser = argparse.ArgumentParser(description='Compute orbital radial density')
    parser.add_argument('--path', type=str, default='.')
    parser.add_argument('--xchem_config', type=str, default='orbitals.json')
    parser.add_argument('--basis', type=str, default='basis_block.raw.info')
    parser.add_argument("--ov", type=str, default='ov.int')
    parser.add_argument('--orbs', type=str, nargs='+', default=['mpcg.bin'])
    parser.add_argument('--rmin', type=float, default=0)
    parser.add_argument('--rmax', type=float, default=20)
    parser.add_argument('--step', type=float, default=0.5)

    if cmd is not None:
        cmd = cmd.split()
    args = parser.parse_args(cmd)
    path = pathlib.Path(args.path)
    xchem_config = json.load(open(path / args.xchem_config))
    config = {'lmono': xchem_config['input']['lMonocentric'],
              'kmono': xchem_config['input']['kMonocentric'],
              'npoly_basis': xchem_config['number_local_basis_functions'],
              'inac_orbs': sum(xchem_config['input']['inac']),
              'npoly_orbs': sum(xchem_config['input']['ras2'] + xchem_config['input']['inac']),
              'norbs': xchem_config['number_diffuse_orbitals'],
              'rmin': args.rmin,
              'rmax': args.rmax,
              'step': args.step,
              'path': path,
              'basis_info': path / args.basis,
              'ov': path / args.ov,
              'orbs': [path / orb for orb in args.orbs]}
    return config


def get_basis_map(filepath):
    """
    From the basis info file generates the map
    (l,m) -> [namedtuple(index, k, exponent)]
    """
    BasisFunction = collections.namedtuple('BasisFunction', 'index k exponent')
    basis_map = {}
    with open(filepath, 'r') as f:
        count = 0
        for line in f:
            l, m, k, exp = [int(x) for x in line.split()[2:6]]
            if (l, m) not in basis_map:
                basis_map[(l, m)] = [BasisFunction(count, k, exp)]
            else:
                basis_map[(l, m)].append(BasisFunction(count, k, exp))
            count = count + 1
    return basis_map


def normalize(v, ov):
    kk=v@ov@v
    if kk>=1e-8:
        return v / np.sqrt(kk)
    else:
        return v*0.


def gram_schmidt(vectors, ov):
    print (vectors.shape)
    A = np.copy(vectors)
    n = A.shape[1]
    A[:, 0] = normalize(A[:, 0], ov)
    for i in range(1, n):
        print ("Orthonormalizing ",i)
        Ai = A[:, i]
        t=Ai @ ov @ A[:,:i]
        for j in range(0, i):
            Ai = Ai - t[j] * A[:,j]
        A[:, i] = normalize(Ai, ov)
    return A


def lowdin(vectors, basis_ov):
    vectors_ov = vectors.T @ basis_ov @ vectors
    eigvals, eigvecs = np.linalg.eigh(vectors_ov)
    S_half = eigvecs @ np.diag(eigvals**(-.5)) @ eigvecs.T
    return np.transpose(S_half @ vectors.T)


def orthonormalize_basis(overlap, threshold=1e-8):
    """Diagonalizes input basis and removes linear dependencies"""
    eigvals, eigvecs = np.linalg.eigh(overlap)
    mask = eigvals > threshold*eigvals[-1]
    return eigvecs[:, mask] / np.sqrt(eigvals[mask])


def read_orbitals(filepath):
    """ Read orbitals matrix from a fortran file"""
    f = scipy.io.FortranFile(filepath, 'r')
    nsym = f.read_ints(dtype=np.int64)[0]
    nbas = f.read_ints(dtype=np.int64)[0]

    orbitals = f.read_reals().reshape((nbas, nbas), order='F')
    f.close()
    return orbitals


def write_orbitals_to_mpcg(filepath, orbitals):
    """Write orbitals matrix to a binary fortran readable file in mpcg.bin format"""
    f = scipy.io.FortranFile(filepath, 'w')
    nbas = np.shape(orbitals)[0]
    f.write_record(1)
    f.write_record(nbas)
    f.write_record(orbitals.T)
    f.close()


def write_orbitals_to_inporb(filepath, orbitals):
    """Write orbitals matrix to an ASCII with INPORB format for MOLCAS"""
    def print_ncol_lines(ncols, fmt, f, arr):
        for i, value in enumerate(arr):
            if i % ncols == 0 and i != 0:
                f.write('\n')
            f.write(f' {value: {fmt}}')
        f.write('\n')
                
    with open(filepath, 'w') as f:
        nsym = 1
        nbas, norbs = orbitals.shape
        f.write('#INPORB 2.2\n')
        f.write('#INFO\n')
        f.write('* Symmetry Specifications\n')
        f.write(f' {0:7d} {nsym:7d} {0:7d}\n')
        f.write(f' {nbas:7d}\n')
        f.write(f' {nbas:7d}\n')
        f.write('* Written by Pedro without symmetry\n')
        f.write('#ORB\n')
        isym = 1
        for iorb in range(nbas):
            #f.write(f'* ORBITAL {isym:4d} {iorb+1:4d} {iorb+1:4d}\n')
            f.write(f'* ORBITAL {isym:4d} {iorb+1:4d}\n')
            if iorb < norbs:
                print_ncol_lines(5, '21.14E', f, orbitals[:, iorb])
            else:
                print_ncol_lines(5, '21.14E', f, np.zeros(nbas))
        f.write('#OCC\n')
        f.write('* OCCUPATION NUMBERS\n')
        print_ncol_lines(5, '21.14E', f, np.zeros(nbas))
        
        f.write('#OCHR\n')
        f.write('* OCCUPATION NUMBERS (HUMAN-READABLE)\n')
        print_ncol_lines(10, '7.4f', f, np.zeros(nbas))
        
        f.write('#ONE\n')
        f.write('* ONE ELECTRON ENERGIES\n')
        print_ncol_lines(10, '11.4E', f, np.zeros(nbas))
        

def read_overlap(filepath):
    """Read overlap matrix from a fortran file"""
    f = scipy.io.FortranFile(filepath, 'r')
    nsym = f.read_ints()[0]
    nbas = f.read_ints()[0]

    overlap = np.zeros((nbas, nbas), order='F')
    for ibas in range(nbas):
        overlap[0:ibas+1, ibas] = f.read_reals()

    f.close()
    overlap = overlap + overlap.T - np.diag(np.diag(overlap))
    return overlap
