# XCHEM tools

Tools to work with XCHEM orbitals.

- utils.py: module with general functions used by the rest of scripts.
- orb_transform.py: transoform the orbitals from XCHEM (mpcg.bin) by projecting polycentric basis into monocentric basis.
- mgi.py: takes transformed orbitals by orb_transform.py and integrates the density of the orbitals at different radial distances.
- orbmono_dens: get the radial distribution for orbitals using only the monocentric basis.
- get_rydberg_orbital.py: get rydberg states from H$_{qc}$ submatrix in MOLCAS INPORB format and outputs dipoles with ground state.

