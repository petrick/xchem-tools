"""
Compute monocentric radial density
for a set orbitals coming from a xchem calculation
"""
import numpy as np
import scipy.special

import utils


def eval_gtos_at_r(r, l_xchem, k_xchem, nexps=22, alpha0=0.01, beta=1.46):
    """Eval GTOs at specified radial position"""
    nmax = max(l_xchem) + 2*get_kmax(max(l_xchem), k_xchem)
    gtos = np.zeros((nmax+1, nexps))
    for n in range(nmax+1):
        for iexp in range(nexps):
            alpha = alpha0 * beta**iexp
            norm_factor = np.sqrt(2*(2*alpha)**(n + 3/2)
                                  / scipy.special.gamma(n + 3/2))
            gtos[n, iexp] = norm_factor * r**n * np.exp(-alpha * r**2)
    return gtos


def get_kmax(l, k_list):
    if len(k_list) == 1:
        kmax = k_list[0]
    else:
        kmax = k_list[l]
    return kmax


def build_gto_product(evaluated_gtos, l, basisfuncs):
    """Compute product matrix of pair GTOs evaluated at r"""
    nbas = len(basisfuncs)
    gto_prod = np.zeros((nbas, nbas))
    for i in range(nbas):
        i_l2k = l + 2*basisfuncs[i].k
        iexp = basisfuncs[i].exponent - 1
        for j in range(nbas):
            j_l2k = l + 2*basisfuncs[j].k
            jexp = basisfuncs[j].exponent - 1
            gto_prod[i, j] = evaluated_gtos[i_l2k, iexp] * evaluated_gtos[j_l2k, jexp]
    return gto_prod


def get_lm_orbitals(orbitals, basis):
    nbas = len(basis)
    norbs = orbitals.shape[1]
    lm_orbitals = np.zeros((nbas, norbs))
    for i, bas in enumerate(basis):
        lm_orbitals[i, :] = orbitals[bas.index, :]
    return lm_orbitals


def compute_radial_densities(l_xchem, k_xchem, lm_map, orbitals, r_distances):
    """Compute radial density for all orbitals"""
    l_prev = -1
    density = np.zeros((len(r_distances), orbitals.shape[1]))
    for irad, r in enumerate(r_distances):
        print(r)
        gtos_at_r = eval_gtos_at_r(r, l_xchem, k_xchem)
        for lm, basis in lm_map.items():
            l, m = lm
            lm_orbitals = get_lm_orbitals(orbitals, basis)
            if l_prev != l:
                gto_product = build_gto_product(gtos_at_r, l, basis)
            density[irad, :] += np.diag(lm_orbitals.T @ gto_product @ lm_orbitals) * r**2
            l_prev = l
    return density


def write_gnuplot_matrix(fp, x, y, z):
    """Write matrix in gnuplot format"""
    ncols = len(x)
    nrows = len(y)
    fp.write(str(ncols))
    for j in range(ncols):
        fp.write(f'  {x[j]}')
    fp.write('\n')
    for i in range(nrows):
        fp.write(str(y[i]))
        for j in  range(ncols):
            fp.write(f'  {z[i, j]}')
        fp.write('\n')
    fp.write('\n\n')


def main():
    config = utils.get_config()
    lm_map = utils.get_basis_map(config['basis_info'])
    orbs = [utils.read_orbitals(orb)[config['npoly_basis']:, 0:config['norbs']]
            for orb in config['orbs']]
    r_distances = np.arange(config['rmin'], config['rmax']+config['step'], config['step'])
    densities = []
    for orb in orbs:
        densities.append(compute_radial_densities(config['lmono'], config['kmono'], lm_map, orb, r_distances))

    for dens in densities:
        with open('densities_gnuplot.txt', 'w') as f:
            write_gnuplot_matrix(f, range(1, 672), r_distances, dens)
        np.savez('densities.npz', r_distance=r_distances, density=dens)


if __name__ == '__main__':
    main()
