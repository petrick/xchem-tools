"""
Compute polycentric orbitals integral at different from R0 to different Rf
"""

import sys
from functools import cache
import numpy as np
from scipy.special import gammainc, gamma

import utils


def get_exponent_value(n, alpha_0=0.01, beta=1.46):
    """Generate exponents according to geometric series alpha_n = alpha_0 * beta**n for n >= 1"""

    return alpha_0 * beta**(n-1)


def lower_incomplete_gamma(a, x):
    return gammainc(a, x) * gamma(a)


@cache
def overlap_integral(l, basisfunc1, basisfunc2, rf):
    alpha1 = get_exponent_value(basisfunc1.exponent)
    n1 = l + 2*basisfunc1.k
    prefactor1 = np.sqrt(2*(2*alpha1)**(n1 + 3/2) / gamma(n1 + 3/2))

    alpha2 = get_exponent_value(basisfunc2.exponent)
    n2 = l + 2*basisfunc2.k
    prefactor2 = np.sqrt(2*(2*alpha2)**(n2 + 3/2) / gamma(n2 + 3/2))

    alpha = alpha1 + alpha2
    n = n1 + n2
    prefactor = prefactor1 * prefactor2
    return lower_incomplete_gamma((n+3)/2, alpha*rf*rf) / (2*alpha**((n+3)/2)) * prefactor


def build_partial_overlap(l, lm_basisfuncs, rf):
    nbasis = len(lm_basisfuncs)
    partial_overlap = np.zeros((nbasis, nbasis))
    for ibas in range(nbasis):
        igto = lm_basisfuncs[ibas]
        for jbas in range(nbasis):
            jgto = lm_basisfuncs[jbas]
            partial_overlap[ibas, jbas] = overlap_integral(l, igto, jgto, rf)
    return partial_overlap


def build_partial_orbs(basisfuncs, orbitals):
    nbasis = len(basisfuncs)
    norbs = orbitals.shape[1]
    partial_orbs = np.zeros((nbasis, norbs))
    for i in range(nbasis):
        partial_orbs[i, :] = orbitals[basisfuncs[i].index, :]
    return partial_orbs


def main():
    config = utils.get_config(' '.join(sys.argv[1:]))
    r_distances = np.arange(0, config['rmax']+config['step'], config['step'])
    partial_integrals = np.zeros((len(r_distances), config['npoly_orbs']))
    lm_map = utils.get_basis_map(config['basis_info'])
    ov = utils.read_overlap(config['ov'])[config['npoly_basis']:, config['npoly_basis']:]
    orbs = utils.read_orbitals(config['orbs'][0])[config['npoly_basis']:, 0:config['npoly_orbs']]
    inf_integrals = np.diag(orbs.T @ ov @ orbs)
    for l in range(max(config['lmono'])+1):
        for m in range(-l, l+1):
            print(f'l = {l}, m = {m}')
            partial_orbs = build_partial_orbs(lm_map[l, m], orbs)
            partial_overlap = np.zeros((len(lm_map[l,m]), len(lm_map[l, m])))
            for i, rf in enumerate(r_distances[1:], start=1):
                print(f'R0 = {rf}')
                partial_overlap = build_partial_overlap(l, lm_map[(l, m)], rf)
                partial_integrals[i, :] += np.diag(partial_orbs.T @ partial_overlap @ partial_orbs)

    np.savetxt("polyorbs_R0.txt", np.hstack((r_distances[:, np.newaxis], inf_integrals[np.newaxis, :] - partial_integrals)))
    np.savetxt("partial_R0.txt", np.hstack((r_distances[:, np.newaxis], partial_integrals)))


if __name__ == '__main__':
    main()
