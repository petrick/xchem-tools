#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import argparse

import numpy as np

import utils


def main():
    parser = argparse.ArgumentParser(
        description='Generate INPORB of rydberg orbitals'
    )
    parser.add_argument('--parents', type=int, nargs='+', default=[])
    parser.add_argument('--gs_energy', type=float, default=0.)
    parser.add_argument('--inactive_orbs', type=int, nargs='+', default=[])
    args = parser.parse_args()
    
    for parent in args.parents:
        hamFile = f'Ham_00{parent}-00{parent}.out'
        ovFile = f'ov_00{parent}-00{parent}.out'
        
        ham_augment_basis = np.loadtxt(hamFile, skiprows=1)
        ov_augment_basis = np.loadtxt(ovFile, skiprows=1)
        
        ovVals, ovVecs = np.linalg.eigh(ov_augment_basis)
        orthonorm_basis = ovVecs @ np.diag(ovVals**(-0.5))
        
        ham_orthobasis = orthonorm_basis.T @ ham_augment_basis @ orthonorm_basis
        hamVals, hamVecs = np.linalg.eigh(ham_orthobasis)
        
        hamVecs_augment_basis = orthonorm_basis @ hamVecs
        
        active_orbs = ov_augment_basis.shape[0]
        mpcg_all_orbs = utils.read_orbitals('mpcg.bin')
        nactive_orbs = ov_augment_basis.shape[0]
        norbs = nactive_orbs + len(args.inactive_orbs)
        active_orbs = [
            iorb 
            for iorb in range(norbs)
            if iorb+1 not in args.inactive_orbs
        ]
        mpcg_active_orbs = mpcg_all_orbs[:, active_orbs]
        hamVecs_primitive_basis = mpcg_active_orbs @ hamVecs_augment_basis
        utils.write_orbitals_to_inporb(
            f'parent{parent}.RasOrb',
            hamVecs_primitive_basis
        )
        
        output_matrix = np.zeros((hamVals.size, 5))
        output_matrix[:, 0] = hamVals
        output_matrix[:, 1] = (hamVals - args.gs_energy)*27.211
        for idip in range(1, 4):
            dipFile = f'dip{idip}_000-00{parent}.out'
            dip = np.loadtxt(dipFile, skiprows=1)
            dip_hambasis = hamVecs.T @ orthonorm_basis.T @ dip
            output_matrix[:, idip+1] = dip_hambasis
            
        np.savetxt(f'parent{parent}.data', output_matrix, '%12.8f')


if __name__ == '__main__':
    main()